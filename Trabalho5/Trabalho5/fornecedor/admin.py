from django.contrib import admin
from .models import Fornecedor

class FornecedorAdmin(admin.ModelAdmin):
    fields = ('slug', 'nome', 'endereco', 'telefone', 'cnpj')
    list_display = ['slug', 'nome', 'endereco', 'telefone', 'cnpj']
    search_fields = ['nome']
    list_filter = ['cnpj']
    list_editable = ['nome', 'endereco', 'telefone', 'cnpj']
    prepopulated_fields = {'slug': ('nome',)}

admin.site.register(Fornecedor, FornecedorAdmin)
