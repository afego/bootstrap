from django.db import models


class Fornecedor(models.Model):
    nome = models.CharField(max_length=50, db_index=True, unique=True)
    endereco = models.CharField(max_length=100)
    telefone = models.CharField(max_length=10)
    cnpj = models.IntegerField()
    slug = models.SlugField(max_length=50)

    class Meta:
        db_table = 'fornecedor'
        ordering = ('nome',)

    def __str__(self):
        return self.nome
