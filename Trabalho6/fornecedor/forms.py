from django import forms
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from fornecedor.models import Fornecedor
from projeto import settings


class PesquisaFornecedorForm(forms.Form):

    nome = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'maxlength': '100'}),
        required=False)


class FornecedorForm(forms.ModelForm):

    class Meta:
        model = Fornecedor
        fields = ('nome', 'endereco', 'telefone', 'cnpj')
        localized_fields = ('cnpj',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['nome'].error_messages={'required': 'Campo obrigatório.',
                                            'unique': 'Nome de fornecedor duplicado.'}
        self.fields['nome'].widget.attrs.update({'class': 'form-control form-control-sm'})

        self.fields['endereco'].error_messages={'required': 'Campo obrigatório'}
        self.fields['endereco'].widget.attrs.update({'class': 'form-control form-control-sm'})

        self.fields['telefone'].error_messages = {'required': 'Campo obrigatório'}
        self.fields['telefone'].widget.attrs.update({'class': 'form-control form-control-sm'})

        self.fields['cnpj'].error_messages = {'required': 'Campo obrigatório',
                                              'invalid': 'CNPJ invalido'}
        self.fields['cnpj'].widget.attrs.update({'class': 'form-control form-control-sm'})

    def clean_cnpj(self):
        cnpj_lido = self.cleaned_data['cnpj']
        cnpj = str(cnpj_lido)
        if (len(cnpj) != 14):
            raise ValidationError("Quantidade errada de digitos para o CNPJ.")

        digitos_verificadores = cnpj[-2:]  # Pegando os ultimos dois digitos do cnpj
        soma = 0
        lista1 = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
        lista2 = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]

        for i in range(len(lista1)):
            soma = soma + int(cnpj[i]) * int(lista1[i])

        resto = soma % 11
        if (resto < 2):
            digito1 = 0
        else:
            digito1 = 11 - resto

        soma = 0
        for i in range(len(lista2)):
            soma = soma + int(cnpj[i]) * int(lista2[i])

        resto = soma % 11
        if (resto < 2):
            digito2 = 0
        else:
            digito2 = 11 - resto

        if (digitos_verificadores == str(digito1) + str(digito2)):
            return cnpj_lido
        else:
            raise ValidationError("CNPJ invalido.")