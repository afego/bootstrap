Feito por André Fernandes Gonçalves

Componentes extras utilizados:
1- Breadcrumb: Para manter uma organização em qual página está.
2- Badge: Para indicar se um curso ainda possui vagas disponíveis.
3- Pagination: Para navegar os diversos cursos.
4- Panel: Para exibir os professores do curso na página inicial.
5- Alerts: Para identificar a aprovação em faculdades nos depoimentos