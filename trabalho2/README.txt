Feito por André Fernandes Gonçalves

Trabalho 1 - Componentes extras utilizados:
1- Breadcrumb: Para manter uma organização em qual página está.
2- Badge: Para indicar se um curso ainda possui vagas disponíveis.
3- Pagination: Para navegar os diversos cursos.
4- Panel: Para exibir os professores do curso na página inicial.
5- Alerts: Para identificar a aprovação em faculdades nos depoimentos

Trabalho 2 - Inclusão da aba "Nos dê sua opinião" para avaliação do curso.