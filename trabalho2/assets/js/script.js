/// <reference path="../../typings/globals/jquery/index.d.ts" />

$(function() {
   $('[data-toggle="tooltip"]').tooltip();
   $('[data-toggle="popover"]').popover();

   $("#botao-enviar").click(function() {
      let matriculaValida = validaMatriculaFunction()
      let conclusaoValida = validaConclusaoFunction()
      let opiniaoMatematicaValida = validaMatematicaFunction()
      let opiniaoRedacaoValida = validaRedacaoFunction()
      let opiniaoFisicaValida = validaFisicaFunction()
      let opiniaoQuimicaValida = validaQuimicaFunction()
      let aprovacaoValida = validaAprovacaoFunction()

      if (matriculaValida && conclusaoValida && aprovacaoValida && opiniaoMatematicaValida && opiniaoRedacaoValida && opiniaoFisicaValida && opiniaoQuimicaValida) {
         alert("Preenchimento correto.")
      }
      else {
         alert("Erro no preenchimento.")
      }
   })
})

function validaMatriculaFunction() {
   let nome = $("#matricula")

   if (nome.val() == '') {
      nome.addClass("is-invalid")
      nome.removeClass("is-valid")
      return false
   }
   else {
      nome.removeClass("is-invalid")
      nome.addClass("is-valid")
      return true
   }
}

function validaConclusaoFunction() {
   let conc_Inc = $("#conclusaoIncompleta")
   let conc_Com = $("#conclusaoCompleta")

   let botoes = $("input[name='conclusão']:checked")
   if (botoes.length === 0) {
      conc_Inc.addClass("is-invalid")
      conc_Inc.removeClass("is-valid")
      conc_Com.addClass("is-invalid")
      conc_Com.removeClass("is-valid")
      $("#conclusaoFeedback").addClass("d-block")
      return false
   }
   else {
      conc_Inc.removeClass("is-invalid")
      conc_Inc.addClass("is-valid")
      conc_Com.removeClass("is-invalid")
      conc_Com.addClass("is-valid")
      $("#conclusaoFeedback").removeClass("d-block")
      return true
   }
}

function validaMatematicaFunction() {
   let avaliacao = $("#matematica")

   if(avaliacao.val() === '') {
      avaliacao.addClass("is-invalid")
      avaliacao.removeClass("is-valid")
      return false
   }
   else {
      avaliacao.removeClass("is-invalid")
      avaliacao.addClass("is-valid")
      return true
   }
}

function validaRedacaoFunction() {
   let avaliacao = $("#redacao")

   if(avaliacao.val() === '') {
      avaliacao.addClass("is-invalid")
      avaliacao.removeClass("is-valid")
      return false
   }
   else {
      avaliacao.removeClass("is-invalid")
      avaliacao.addClass("is-valid")
      return true
   }
}

function validaFisicaFunction() {
   let avaliacao = $("#fisica")

   if(avaliacao.val() === '') {
      avaliacao.addClass("is-invalid")
      avaliacao.removeClass("is-valid")
      return false
   }
   else {
      avaliacao.removeClass("is-invalid")
      avaliacao.addClass("is-valid")
      return true
   }
}

function validaQuimicaFunction() {
   let avaliacao = $("#quimica")

   if(avaliacao.val() === '') {
      avaliacao.addClass("is-invalid")
      avaliacao.removeClass("is-valid")
      return false
   }
   else {
      avaliacao.removeClass("is-invalid")
      avaliacao.addClass("is-valid")
      return true
   }
}

function validaAprovacaoFunction() {
   let aprovado = $("#aprovado")
   let naoAprovado = $("#naoAprovado")

   let aprovacaoFeedback = $("#aprovacaoFeedback")

   let botoes = $("input.aprovacao:checked")
   if (botoes.length === 0) {
      aprovado.addClass("is-invalid")
      aprovado.removeClass("is-valid")
      naoAprovado.addClass("is-invalid")
      naoAprovado.removeClass("is-valid")
      aprovacaoFeedback.addClass("d-block")
      return false
   }
   else {
      aprovado.removeClass("is-invalid")
      aprovado.addClass("is-valid")
      naoAprovado.removeClass("is-invalid")
      naoAprovado.addClass("is-valid")
      aprovacaoFeedback.removeClass("d-block")
      return true
   }
}












